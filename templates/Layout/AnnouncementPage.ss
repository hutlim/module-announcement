<% include SideBar %>
<div class="typography col-sm-8">
    <div class="well">
        $Message
    	<article>
    		<h1>$Title</h1>
    		<div class="list-group">
    			<% if $Announcements %>
    				<% loop $Announcements %>
					    	<a class="list-group-item" href="$Link">
					    		<% if not $Top.Read($ID) %>
						    		<span class="badge"><%t AnnouncementPage.ss.UNREAD "Unread" %></span>
						    	<% end_if %>
					    		<h4 class="list-group-item-heading">$Title</h4>
					    		<p class="list-group-item-text"><i class="fa fa-calendar-o"></i> $PublishDate.Nice - $Content.ContextSummary(200)</p>
					    	</a>
					<% end_loop %>
				<% else %>
					<div class="list-group-item">
						<p class="list-group-item-text"><%t AnnouncementPage.ss.NO_ANNOUNCEMENT "No announcement at this moment" %></p>
					</div>
				<% end_if %>
			</div>
			<% if $Announcements.MoreThanOnePage %>
	            <div class="text-center">
	                <ul class="pagination">
		                <% if $Announcements.NotFirstPage %>
		                    <li><a href="$Announcements.PrevLink"><%t AnnouncementPage.ss.PREV "Prev" %></a></li>
		                <% else %>
		                    <li class="disabled"><a href="#"><%t AnnouncementPage.ss.PREV "Prev" %></a></li>
		                <% end_if %>
		                <% loop $Announcements.Pages(5) %>
		                    <% if CurrentBool %>
		                        <li class="active"><a href="#">$PageNum</a></li>
		                    <% else %>
		                        <% if Link %>
		                            <li><a href="$Link">$PageNum</a></li>
		                        <% else %>
		                            <li><a href="#">...</a></li>
		                        <% end_if %>
		                    <% end_if %>
		                <% end_loop %>
		                <% if $Announcements.NotLastPage %>
		                    <li><a href="$Announcements.NextLink"><%t AnnouncementPage.ss.NEXT "NEXT" %></a></li>
		                <% else %>
		                    <li class="disabled"><a href="#"><%t AnnouncementPage.ss.NEXT "Next" %></a></li>
		                <% end_if %>
	                </ul>
	            </div>
	        <% end_if %>
    	</article>
    </div>
</div>