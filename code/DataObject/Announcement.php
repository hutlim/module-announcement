<?php
/**
 * @package announcement
 */
class Announcement extends DataObject implements PermissionProvider {
    private static $singular_name = "Announcement";
    private static $plural_name = "Announcements";
    
    private static $db = array(
    	'PublishDate' => 'Date',
    	'IsActive' => 'Boolean',
    	'IsPopup' => 'Boolean',
    	'Title' => "Varchar(250)",
        'Content' => 'HTMLText'
    );
	
	private static $many_many = array(
        'ViewGroups' => 'Group',
        'IsRead' => 'Member'
    );
	
	private static $defaults = array(
		'IsActive' => 1
	);
	
	private static $default_sort = "PublishDate DESC";

    private static $searchable_fields = array(
    	'PublishDate' => array(
			'field' => 'DateField',
			'filter' => 'DateMatchFilter'
		),
    	'IsActive',
    	'IsPopup',
        'Title',
        'Content'
    );

    private static $summary_fields = array(
    	'PublishDate.Nice',
    	'IsActive.Nice',
    	'IsPopup.Nice',
        'Title',
        'Content.Summary',
        'ViewGroupsText'
    );
	
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['PublishDate'] = _t('Announcement.PUBLISHED_DATE', 'Published Date');
		$labels['PublishDate.Nice'] = _t('Announcement.PUBLISHED_DATE', 'Published Date');
		$labels['IsActive'] = _t('Announcement.IS_ACTIVE', 'Is Active?');
		$labels['IsActive.Nice'] = _t('Announcement.IS_ACTIVE', 'Is Active?');
		$labels['IsPopup'] = _t('Announcement.IS_POPUP', 'Is Popup?');
		$labels['IsPopup.Nice'] = _t('Announcement.IS_POPUP', 'Is Popup?');
		$labels['Title'] = _t('Announcement.TITLE', 'Title');
		$labels['Content'] = _t('Announcement.CONTENT', 'Content');
		$labels['Content.Summary'] = _t('Announcement.CONTENT', 'Content');
		$labels['ViewGroups'] = _t('Announcement.VIEW_GROUPS', 'View Groups');
		$labels['ViewGroupsText'] = _t('Announcement.VIEW_GROUPS', 'View Groups');
		$labels['IsRead'] = _t('Announcement.IS_READ', 'Is Read Member?');
		
		return $labels;	
	}
	
	function getCMSFields() {
        $fields = parent::getCMSFields();
		
		$groups = Group::get()->filter('IsDistributorGroup', 1);
		
		$fields->removeByName('ViewGroups');
    	$fields->addFieldToTab('Root.Main', $group_list = ListboxField::create('ViewGroups', $this->fieldLabel('ViewGroups'), $groups->map('ID', 'Breadcrumbs')->toArray())->setMultiple(true));
		
		if(!$this->exists()){
			$group_list->setValue(array_values($groups->getIDList()));
		}
        
        return $fields;
    }
	
	function Link(){
		$page = AnnouncementPage::get_one('AnnouncementPage');
		if($page){
			return Controller::join_links($page->Link('view'), $this->ID);
		}
		
		return '#';
	}
	
	function ViewGroupsText(){
		return implode(', ', $this->ViewGroups()->map()->toArray());
	}
    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_Announcement');
    }

    function canEdit($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('EDIT_Announcement');
    }

    function canDelete($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('DELETE_Announcement');
    }

    function canCreate($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('CREATE_Announcement');
    }

    public function providePermissions() {
        return array(
            'VIEW_Announcement' => array(
                'name' => _t('Announcement.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('Announcement.PERMISSIONS_CATEGORY', 'Announcement')
            ),

            'EDIT_Announcement' => array(
                'name' => _t('Announcement.PERMISSION_EDIT', 'Allow edit access right'),
                'category' => _t('Announcement.PERMISSIONS_CATEGORY', 'Announcement')
            ),

            'DELETE_Announcement' => array(
                'name' => _t('Announcement.PERMISSION_DELETE', 'Allow delete access right'),
                'category' => _t('Announcement.PERMISSIONS_CATEGORY', 'Announcement')
            ),

            'CREATE_Announcement' => array(
                'name' => _t('Announcement.PERMISSION_CREATE', 'Allow create access right'),
                'category' => _t('Announcement.PERMISSIONS_CATEGORY', 'Announcement')
            ),
        );
    }
}
?>