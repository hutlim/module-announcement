<?php
/**
 * Bulk action handler for activate/deactivate, enable/disable popup announcement
 * 
 * @package announcement
 */
class GridFieldBulkAnnouncementHandler extends GridFieldBulkHandler
{	
	/**
	 * RequestHandler allowed actions
	 * @var array
	 */
	private static $allowed_actions = array('activate', 'deactivate', 'enable', 'disable', 'delete');


	/**
	 * RequestHandler url => action map
	 * @var array
	 */
	private static $url_handlers = array(
		'activate' => 'activate',
		'deactivate' => 'deactivate',
		'enable' => 'enable',
		'disable' => 'disable',
		'delete' => 'delete'
	);
	

	/**
	 * Activate the selected records passed from the activate bulk action
	 * 
	 * @param SS_HTTPRequest $request
	 * @return SS_HTTPResponse List of activated records ID
	 */
	public function activate(SS_HTTPRequest $request){
		$ids = array();
		
		try {
            DB::getConn()->transactionStart();
			foreach ($this->getRecords() as $record){
				if($record->canEdit() && !$record->IsActive){
					array_push($ids, $record->ID);
					$record->IsActive = 1;
	            	$record->write();
				}
			}
            DB::getConn()->transactionEnd();
        }
        catch(ValidationException $e){
            DB::getConn()->transactionRollback();
            throw new ValidationException($e->getMessage(), 0);
        }

		$response = new SS_HTTPResponse(Convert::raw2json(array(
			'result' => 'success',
			'message' => _t('GridFieldBulkAnnouncementHandler.SUCCESS_ACTIVATED', 'Total {count} announcement has been activated', '', array('count' => sizeof($ids)))
		)));
		$response->addHeader('Content-Type', 'text/json');
		return $response;	
	}
	
	/**
	 * Deactivate the selected records passed from the deactivate bulk action
	 * 
	 * @param SS_HTTPRequest $request
	 * @return SS_HTTPResponse List of deactivated records ID
	 */
	public function deactivate(SS_HTTPRequest $request){
		$ids = array();
		
		try {
            DB::getConn()->transactionStart();
			foreach ($this->getRecords() as $record){
				if($record->canEdit() && $record->IsActive){
					array_push($ids, $record->ID);
					$record->IsActive = 0;
	            	$record->write();
				}
			}
            DB::getConn()->transactionEnd();
        }
        catch(ValidationException $e){
            DB::getConn()->transactionRollback();
            throw new ValidationException($e->getMessage(), 0);
        }

		$response = new SS_HTTPResponse(Convert::raw2json(array(
			'result' => 'success',
			'message' => _t('GridFieldBulkAnnouncementHandler.SUCCESS_DEACTIVATED', 'Total {count} announcement has been deactivated', '', array('count' => sizeof($ids)))
		)));
		$response->addHeader('Content-Type', 'text/json');
		return $response;	
	}
	
	/**
	 * Enable popup the selected records passed from the enable bulk action
	 * 
	 * @param SS_HTTPRequest $request
	 * @return SS_HTTPResponse List of enabled records ID
	 */
	public function enable(SS_HTTPRequest $request){
		$ids = array();
		
		try {
            DB::getConn()->transactionStart();
			foreach ($this->getRecords() as $record){
				if($record->canEdit() && !$record->IsPopup){
					array_push($ids, $record->ID);
					$record->IsPopup = 1;
	            	$record->write();
				}
			}
            DB::getConn()->transactionEnd();
        }
        catch(ValidationException $e){
            DB::getConn()->transactionRollback();
            throw new ValidationException($e->getMessage(), 0);
        }

		$response = new SS_HTTPResponse(Convert::raw2json(array(
			'result' => 'success',
			'message' => _t('GridFieldBulkAnnouncementHandler.SUCCESS_ENABLED_POPUP', 'Total {count} announcement has been enabled popup', '', array('count' => sizeof($ids)))
		)));
		$response->addHeader('Content-Type', 'text/json');
		return $response;	
	}
	
	/**
	 * Disable popup the selected records passed from the disable bulk action
	 * 
	 * @param SS_HTTPRequest $request
	 * @return SS_HTTPResponse List of disabled records ID
	 */
	public function disable(SS_HTTPRequest $request){
		$ids = array();
		
		try {
            DB::getConn()->transactionStart();
			foreach ($this->getRecords() as $record){
				if($record->canEdit() && $record->IsPopup){
					array_push($ids, $record->ID);
					$record->IsPopup = 0;
	            	$record->write();
				}
			}
            DB::getConn()->transactionEnd();
        }
        catch(ValidationException $e){
            DB::getConn()->transactionRollback();
            throw new ValidationException($e->getMessage(), 0);
        }

		$response = new SS_HTTPResponse(Convert::raw2json(array(
			'result' => 'success',
			'message' => _t('GridFieldBulkAnnouncementHandler.SUCCESS_DISABLED_POPUP', 'Total {count} announcement has been disabled popup', '', array('count' => sizeof($ids)))
		)));
		$response->addHeader('Content-Type', 'text/json');
		return $response;	
	}
	
	/**
	 * Delete the selected records passed from the delete bulk action
	 * 
	 * @param SS_HTTPRequest $request
	 * @return SS_HTTPResponse List of deleted records ID
	 */
	public function delete(SS_HTTPRequest $request){
		$ids = array();
		
		try {
            DB::getConn()->transactionStart();
			foreach ($this->getRecords() as $record){
				if($record->canDelete()){
	            	array_push($ids, $record->ID);
					$record->delete();
				}
			}
            DB::getConn()->transactionEnd();
        }
        catch(ValidationException $e){
            DB::getConn()->transactionRollback();
            throw new ValidationException($e->getMessage(), 0);
        }

		$response = new SS_HTTPResponse(Convert::raw2json(array(
			'result' => 'success',
			'message' => _t('GridFieldBulkAnnouncementHandler.SUCCESS_DELETED', 'Total {count} announcement has been deleted', '', array('count' => sizeof($ids)))
		)));
		$response->addHeader('Content-Type', 'text/json');
		return $response;	
	}
}