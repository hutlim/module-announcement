<?php
/**
 * @package announcement
 */
class AnnouncementGridFieldPopupAction implements GridField_ColumnProvider, GridField_ActionProvider {
    public function augmentColumns($gridField, &$columns) {
        if(!in_array('Actions', $columns)) {
            $columns[] = 'Actions';
        }
    }

    public function getColumnAttributes($gridField, $record, $columnName) {
        return array('class' => 'col-buttons');
    }

    public function getColumnMetadata($gridField, $columnName) {
        if($columnName == 'Actions') {
            return array('title' => '');
        }
    }

    public function getColumnsHandled($gridField) {
        return array('Actions');
    }

    public function getColumnContent($gridField, $record, $columnName) {
    	Requirements::css('announcement/css/AnnouncementGridFieldPopupAction.css');
        if($record->canEdit()) {
			if(!$record->IsPopup){
        		$field = GridField_FormAction::create($gridField, 'EnablePopup' . $record->ID, false, "enable_popup", array('RecordID' => $record->ID))->addExtraClass('gridfield-button-enable-popup')->setAttribute('title', _t('AnnouncementGridFieldPopupAction.BUTTONENABLEPOPUP', 'Enable Popup'))->setAttribute('data-icon', 'chain--plus')->setDescription(_t('AnnouncementGridFieldPopupAction.ENABLE_POPUP', 'Enable Popup'));
			}
			else{
            	$field = GridField_FormAction::create($gridField, 'DisablePopup' . $record->ID, false, "disable_popup", array('RecordID' => $record->ID))->addExtraClass('gridfield-button-disable-popup')->setAttribute('title', _t('AnnouncementGridFieldPopupAction.BUTTONDISABLEPOP', 'Disable Popup'))->setAttribute('data-icon', 'chain--minus')->setDescription(_t('AnnouncementGridFieldPopupAction.DISABLE_POPUP', 'Disable Popup'));
			}
			
			return $field->Field();
        }
    }

    public function getActions($gridField) {
        return array(
            'enable_popup',
            'disable_popup'
        );
    }

    public function handleAction(GridField $gridField, $actionName, $arguments, $data) {
        if($actionName == 'enable_popup' || $actionName = 'disable_popup') {
            $item = $gridField->getList()->byID($arguments['RecordID']);
            if(!$item) {
                return;
            }
			
			if(!$item->canEdit()){
				throw new ValidationException(_t('AnnouncementGridFieldPopupAction.ACTION_PERMISSION', 'No permission to perform action for these item'), 0);
			}
			else{
				if($actionName == 'enable_popup') {
	                $item->IsPopup = 1;
	            	$item->write();
	            }
				
				if($actionName == 'disable_popup') {
	                $item->IsPopup = 0;
	            	$item->write();
	            }
			}
        }
    }
}
