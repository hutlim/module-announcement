<?php
/**
 * Announcement administration interface, based on ModelAdmin
 * @package announcement
 */
class AnnouncementAdmin extends GeneralModelAdmin {

    private static $url_segment = 'announcement';
    private static $menu_title = 'Announcement';
    private static $menu_icon = 'announcement/images/announcement-icon.png';

    private static $managed_models = array(
    	'Announcement'
    );
	
	public function getEditForm($id = null, $fields = null) {
        $list = $this->getList();
        if(ClassInfo::exists('GridFieldExportToExcelButton')){
        	$exportButton = new GridFieldExportToExcelButton('buttons-after-left');
		}
		else{
			$exportButton = new GridFieldExportButton('buttons-after-left');
		}
        $exportButton->setExportColumns($this->getExportFields());
        
        $listField = GridField::create(
            $this->sanitiseClassName($this->modelClass),
            false,
            $list,
            $fieldConfig = GridFieldConfig_RecordEditor::create($this->stat('page_length'))
                ->removeComponentsByType('GridFieldFilterHeader')
                ->addComponents(new AnnouncementGridFieldActivateAction(), new AnnouncementGridFieldPopupAction(), new GridFieldButtonRow('after'), new GridFieldPrintButton('buttons-after-left'), $exportButton)
        );
		
		if(Permission::check('EDIT_Announcement') || Permission::check('DELETE_Announcement')){
			$fieldConfig->addComponent($bulkButton = new GridFieldBulkAction(), 'GridFieldSortableHeader');
			if(Permission::check('EDIT_Announcement')){
				$bulkButton
				->addBulkAction('activate', _t('AnnouncementAdmin.ACTIVATE', 'Activate'), 'GridFieldBulkAnnouncementHandler')
				->addBulkAction('deactivate', _t('AnnouncementAdmin.DEACTIVATE', 'Deactivate'), 'GridFieldBulkAnnouncementHandler', array('icon' => 'cross-circle'))
				->addBulkAction('enable', _t('AnnouncementAdmin.ENABLE_POPUP', 'Enable Popup'), 'GridFieldBulkAnnouncementHandler', array('icon' => 'chain--plus'))
				->addBulkAction('disable', _t('AnnouncementAdmin.DISABLE_POPUP', 'Disable Popup'), 'GridFieldBulkAnnouncementHandler', array('icon' => 'chain--minus'));
			}
			
			if(Permission::check('DELETE_Announcement')){
				$bulkButton->addBulkAction('delete', _t('AnnouncementAdmin.DELETE', 'Delete'), 'GridFieldBulkAnnouncementHandler', array('icon' => 'decline'));
			}
		}
        
        // Validation
        if(singleton($this->modelClass)->hasMethod('getCMSValidator')) {
            $detailValidator = singleton($this->modelClass)->getCMSValidator();
            $listField->getConfig()->getComponentByType('GridFieldDetailForm')->setValidator($detailValidator);
        }

        $form = CMSForm::create( 
            $this,
            'EditForm',
            new FieldList($listField),
            new FieldList()
        )->setHTMLID('Form_EditForm');
		$form->setResponseNegotiator($this->getResponseNegotiator());
        $form->addExtraClass('cms-edit-form cms-panel-padded center');
        $form->setTemplate($this->getTemplatesWithSuffix('_EditForm'));
        $editFormAction = Controller::join_links($this->Link($this->sanitiseClassName($this->modelClass)), 'EditForm');
        $form->setFormAction($editFormAction);
        $form->setAttribute('data-pjax-fragment', 'CurrentForm');

        $this->extend('updateEditForm', $form);
        
        return $form;
    }
}
?>