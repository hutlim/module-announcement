<?php

class Announcement_ControllerExtension extends Extension {
	private static $casting = array(
		'TotalUnreadAnnouncement' => 'Int'
	);
	
	function getAnnouncementPage(){
        $page = AnnouncementPage::get()->find('ClassName', 'AnnouncementPage');
        if($page) return $page;
    }
    
    function getAnnouncementLink() {
        $page = $this->getAnnouncementPage();
        if($page) return $page->Link();
    }
}
