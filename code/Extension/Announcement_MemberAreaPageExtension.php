<?php

class Announcement_MemberAreaPageExtension extends Extension {
	function onAfterInit(){
        if($this->owner->CurrentMember() && !$this->owner->redirectedTo()){
        	$announcement = Announcement::get()->filter('IsActive', 1)->filter('PublishDate:LessThanOrEqual', date('Y-m-d'))->filter('ViewGroups.ID', $this->owner->CurrentMember()->RankID)->filter('IsPopup', 1)->first();
			if($announcement && $announcement->IsRead()->filter('IsRead.ID', $this->owner->CurrentMember()->ID)->count() == 0){
				Requirements::javascriptTemplate("announcement/javascript/Announcement.js", array('Title' => $announcement->Title, 'Content' => $announcement->dbObject('Content')->XML()));
				$announcement->IsRead()->add($this->owner->CurrentMember()->ID);
			}
		}
    }
	
	function UnreadAnnouncements($limit = 5){
    	$announcements = Announcement::get()->filter('IsActive', 1)->filter('PublishDate:LessThanOrEqual', date('Y-m-d'))->filter('ViewGroups.ID', $this->owner->CurrentMember()->RankID)->limit($limit);
		$exclude = array();
		foreach($announcements as $announcement){
			if($announcement->IsRead()->filter('IsRead.ID', $this->owner->CurrentMember()->ID)->count()){
				$exclude[] = $announcement->ID;
			}
		}
        return $announcements->exclude('ID', $exclude);
    }
	
	function TotalUnreadAnnouncement(){
		$total = 0;
		$announcements = Announcement::get()->filter('IsActive', 1)->filter('PublishDate:LessThanOrEqual', date('Y-m-d'))->filter('ViewGroups.ID', $this->owner->CurrentMember()->RankID);
		foreach($announcements as $announcement){
			if(!$announcement->IsRead()->filter('IsRead.ID', $this->owner->CurrentMember()->ID)->count()){
				$total += 1;
			}
		}
        return $total;
    }
}
