<?php
class AnnouncementPage extends MemberOverviewPage {
	
    private static $db = array();

    private static $has_one = array();

}

class AnnouncementPage_Controller extends MemberOverviewPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array (
        'view'
    );
	
	public function Announcements(){
		$announcements = Announcement::get()->filter('IsActive', 1)->filter('PublishDate:LessThanOrEqual', date('Y-m-d'))->filter('ViewGroups.ID', $this->CurrentMember()->RankID)->sort('PublishDate', 'DESC');
		
		return new PaginatedList($announcements, $this->request);
	}
	
	public function Read($id){
		return Announcement::get()->byID($id)->IsRead()->filter('IsRead.ID', $this->CurrentMember()->ID)->count();
	}
    
    public function view(){
        if($announcement = Announcement::get()->filter('IsActive', 1)->filter('PublishDate:LessThanOrEqual', date('Y-m-d'))->filter('ViewGroups.ID', $this->CurrentMember()->RankID)->byID((int)$this->request->param('ID'))){
        	if(!$this->Read($announcement->ID) && !Distributor::customLoginID()){
        		$announcement->IsRead()->add($this->CurrentMember()->ID);
			}
            return array(
            	'Announcement' => $announcement
            );
        }
        
        return $this->httpError('404', 'Page not found');
    }
}