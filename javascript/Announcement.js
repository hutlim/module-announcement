(function($) {
	// start modal div
	var content = '<div id="announcement_modal" class="modal">';
	
	// start dialog
	content += '<div class="modal-dialog">';
	
	// start content
	content += '<div class="modal-content">';
	
	// header
	content += '<div class="modal-header">';
	content += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
	content += '<h4 class="modal-title">$Title</h4>';
	content += '</div>';
	
	// body
	content += '<div class="modal-body"></div>';
	
	// close content
	content += '</div>';
	
	// close dialog
	content += '</div>';
	
	// close modal div
	content += '</div>';
	$('body').append(content);
	var content = '$Content';
	$('#announcement_modal .modal-body').html($("<div />").html(content).text());
	$('#announcement_modal').modal();
	$('#announcement_modal').modal('show');
})(jQuery);